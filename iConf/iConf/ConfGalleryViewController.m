//
//  ConfGalleryViewController.m
//  iConf
//
//  Created by Daniel Dionisio on 24/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "ConfGalleryViewController.h"
#import "ConferencePageViewController.h"
#import <Restkit/Restkit.h>
#import "Conference.h"
#import "MappingProvider.h"
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "Author.h"
#import "Presentation.h"
#import "AuthorPres.h"
#import "MSCalendarViewController.h"
#import "Map.h"
#import "Event.h"
#import "MenuViewController.h"


@interface ConfGalleryViewController ()
{
    NSMutableArray *userConferences;
    RKManagedObjectStore *managedObjectStore;

    
}

@property (nonatomic, strong) Conference *selectedConf;

@property (weak, nonatomic) IBOutlet UITableView *ConfTable;

@end

@implementation ConfGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) defUser{
    self.user = [User sharedUser];

}

- (void)viewDidLoad
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    [prefs removeObjectForKey:@"UserConf"];
    self.user = [User sharedUser];
    [SVProgressHUD show];
    [self loadDatafromServer];
    self.navigationController.navigationBarHidden=FALSE;
    self.navigationItem.title=@"Conference Gallery";
    self.navigationItem.hidesBackButton = YES;
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return userConferences.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Conference *conf=[userConferences objectAtIndex:indexPath.row];
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
    nameLabel.text=conf.confName;
    
    UILabel *localLabel = (UILabel *)[cell viewWithTag:102];
    localLabel.text=conf.confLocal;
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:103];
    dateLabel.text = conf.date;
    
    UIImageView *avatarImage = (UIImageView *)[cell viewWithTag:100];
    CALayer * l = [avatarImage layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10.0];
    
    // You can even add a border
    [l setBorderWidth:1.0];
    [l setBorderColor:[[UIColor blackColor] CGColor]];
    
    avatarImage.image=[UIImage imageWithData:conf.confImg];
    return cell;
}

- (BOOL)connectedToInternet
{
    NSURL *url=[NSURL URLWithString:@"http://193.136.122.140"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode]==200)?YES:NO;
}


-(void)loadDatafromServer //Codigo do exemplo twitter
{
    
    
    NSURL *url = [NSURL URLWithString:@"http://193.136.122.140"];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:url];
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    manager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"id" :@"confID",
                                            @"conf_date" :@"date",
                                            @"name" : @"confName",
                                            @"hashtag":@"hashtag",
                                            @"local" : @"confLocal",
                                            @"description" :@"confDescription",
                                            @"avatar_url": @"avatar",
                                            };
    
    RKEntityMapping *conferenceMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Conference class]) inManagedObjectStore:manager.managedObjectStore];
    
    
    conferenceMapping.identificationAttributes = @[@"confID"];
    
    [conferenceMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:conferenceMapping pathPattern:nil keyPath:@"conferences" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    //Criar persistencia
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"Conference"];
    }
    
    [manager getObjectsAtPath:[NSString stringWithFormat:@"users/%d/attended.json", self.user.userID] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Conference"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"confID" ascending:YES]];
        userConferences = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
            
        for(Conference *conf=(Conference *)[NSEntityDescription insertNewObjectForEntityForName:@"Conference" inManagedObjectContext:self.user.managedObjectStore.persistentStoreManagedObjectContext] in userConferences){
            
            NSURL *url = [NSURL URLWithString: conf.avatar];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            
            [conf setConfImg:data];
            [managed save:nil];
            
        }
        
        [self.ConfTable reloadData];
        [SVProgressHUD dismiss];

    }
     
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          
                          NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
                          NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Conference"];
                          fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"confID" ascending:YES]];
                          userConferences = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
                          
                          [self.ConfTable reloadData];
                          [SVProgressHUD dismiss];

                      }];
    
}


- (void)loadPresentations
{
    
    NSURL *url = [NSURL URLWithString:@"http://193.136.122.140"];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:url];
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    manager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"id" :@"presID",
                                            @"session_id" :@"sessID",
                                            @"chair" :@"chair",
                                            @"title" :@"presTitle",
                                            @"article" : @"presArticle",
                                            @"room_name" : @"roomName",
                                            @"room_map" : @"roomMap",
                                            @"speaker" : @"speaker",
                                            @"start_at" : @"presStart",
                                            @"end_at" :@"presEnd",
                                            @"duration":@"presDuration",
                                            };
    
    RKEntityMapping *conferenceMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Presentation class]) inManagedObjectStore:manager.managedObjectStore];
    
    
    conferenceMapping.identificationAttributes = @[@"presID"];
    
    [conferenceMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:conferenceMapping pathPattern:nil keyPath:@"results" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    //Criar persistencia
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"Presentation"];
    }
    
    [manager getObjectsAtPath:[NSString stringWithFormat:@"conferences/%d/presentations.json", self.user.sessionConf] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Presentation"];
        NSMutableArray * presMaps = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        
        for(Presentation *pres=(Presentation *)[NSEntityDescription insertNewObjectForEntityForName:@"Presentation" inManagedObjectContext:self.user.managedObjectStore.persistentStoreManagedObjectContext] in presMaps){
            
            NSURL *url = [NSURL URLWithString: pres.roomMap];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            [pres setConfID:[NSString stringWithFormat:@"%d", self.user.sessionConf]];
            [pres setRoomMapImg:data];
            [managed save:nil];
            
            
        }

        
    }
     
    failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          
                          
    }];
    
    
}


- (void)loadAuthors
{
    
    NSURL *url = [NSURL URLWithString:@"http://193.136.122.140"];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:url];
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    manager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"author_id":@"authorId",
                                            @"user_id":@"userId",
                                            @"name":@"authorName",
                                            @"institution":@"authorInstitution",
                                            @"email":@"authorEmail",
                                            @"photo":@"authorPhoto",
                                            };
    
    RKEntityMapping *conferenceMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Author class]) inManagedObjectStore:manager.managedObjectStore];
    
    
    conferenceMapping.identificationAttributes = @[@"authorId"];
    
    [conferenceMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:conferenceMapping pathPattern:nil keyPath:@"authors" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    //Criar persistencia
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"Author"];
    }
    
    [manager getObjectsAtPath:[NSString stringWithFormat:@"conferences/%d/authors.json", self.user.sessionConf] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Author"];
        NSMutableArray * authorImgs = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        
        for(Author *author=(Author *)[NSEntityDescription insertNewObjectForEntityForName:@"Author" inManagedObjectContext:self.user.managedObjectStore.persistentStoreManagedObjectContext] in authorImgs){
            
            NSURL *url = [NSURL URLWithString: author.authorPhoto];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            [author setConfID:[NSString stringWithFormat:@"%d", self.user.sessionConf]];
            [author setAuthorImg:data];
            [managed save:nil];
            
            
        }
        

        
    }
     
    failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          
                          
    }];
    
    
    
}

- (void)loadAuthorsForPres
{
    
    NSURL *url = [NSURL URLWithString:@"http://193.136.122.140"];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:url];
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    manager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"user_id" :@"userId",
                                            @"presentation_id" :@"presId"
                                            };
    
    RKEntityMapping *conferenceMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([AuthorPres class]) inManagedObjectStore:manager.managedObjectStore];
    
    
    conferenceMapping.identificationAttributes = @[@"userId", @"presId"];
    
    [conferenceMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:conferenceMapping pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    //Criar persistencia
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"AuthorPres"];
    }
    
    [manager getObjectsAtPath:@"authors.json" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        
    }
     
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          
                          
                      }];
    
}

- (void)loadMap
{
    
    NSURL *url = [NSURL URLWithString:@"http://193.136.122.140"];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:url];
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    manager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"id" :@"mapId",
                                            @"name" :@"mapName",
                                            @"url" :@"mapUrl"
                                            };
    
    RKEntityMapping *conferenceMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Map class]) inManagedObjectStore:manager.managedObjectStore];
    
    
    conferenceMapping.identificationAttributes = @[@"mapId"];
    
    [conferenceMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:conferenceMapping pathPattern:nil keyPath:@"maps" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    //Criar persistencia
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"Map"];
    }
    
    [manager getObjectsAtPath:[NSString stringWithFormat:@"conferences/%d/maps.json", self.user.sessionConf] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        NSManagedObjectContext *managed=self.user.managedObjectStore.persistentStoreManagedObjectContext;

        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Map"];
        NSMutableArray *storeMap = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        for(Map *map=(Map *)[NSEntityDescription insertNewObjectForEntityForName:@"Map" inManagedObjectContext:self.user.managedObjectStore.persistentStoreManagedObjectContext] in storeMap){
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://193.136.122.140%@", map.mapUrl ]];
            NSData *data = [NSData dataWithContentsOfURL:url];

            
            [map setMapImg:data];
            [managed save:nil];            

            
        }
        
    }
     
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          
                          
                      }];
    
}

- (void)loadEvents
{
    
    NSURL *url = [NSURL URLWithString:@"http://193.136.122.140"];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:url];
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    manager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"id" :@"eventID",
                                            @"duration" :@"duration",
                                            @"name" :@"title",
                                            @"notes" :@"notes",
                                            @"start_at":@"start",
                                            @"conference_id" :@"confID",
                                            @"local" :@"local"
                                            };
    
    RKEntityMapping *conferenceMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Event class]) inManagedObjectStore:manager.managedObjectStore];
    
    
    conferenceMapping.identificationAttributes = @[@"eventID"];
    
    [conferenceMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:conferenceMapping pathPattern:nil keyPath:@"results" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    //Criar persistencia
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"Event"];
    }
    
    [manager getObjectsAtPath:[NSString stringWithFormat:@"conferences/%d/events.json", self.user.sessionConf] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        [SVProgressHUD dismiss];

        
    }
     
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          [SVProgressHUD dismiss];  
                          
                      }];
    
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [SVProgressHUD show];

    ConferencePageViewController *confPageViewController = segue.destinationViewController;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NSIndexPath *indexPath = [self.ConfTable indexPathForSelectedRow];
    self.selectedConf = [userConferences objectAtIndex:indexPath.row];
    User *sessionUser = [User sharedUser];
    sessionUser.sessionConf=[[self.selectedConf valueForKey:@"confID"] intValue];
    MSCalendarViewController *calendar;
    calendar = [[MSCalendarViewController alloc]init];
    [calendar loadSchedule];
    [self loadPresentations];
    [self loadAuthors];
    [self loadAuthorsForPres];
    [self loadMap];
    [self loadEvents];
    //MenuViewController *menu=[[MenuViewController alloc] init];
    sessionUser.managedObjectStore=managedObjectStore;
    confPageViewController.confIdent = self.selectedConf;


}

-(void)deleObjDB:(NSString *)entity{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
    [allCars setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:managed]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [managed executeFetchRequest:allCars error:&error];
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [managed deleteObject:car];
    }
    NSError *saveError = nil;
    [managed save:&saveError];
    
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return NO;
}

@end
