//
//  PresentationArticleViewController.m
//  iConf
//
//  Created by Bruno Candeias on 16/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PresentationArticleViewController.h"
#import "ArticleViewController.h"

@interface PresentationArticleViewController ()

@property (strong, nonatomic) UIDocumentInteractionController *documentInteractionController;

@end

@implementation PresentationArticleViewController

//@synthesize previewContainer;
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self){
       // Custom initialization
    }
    return self;
}
 
    
- (void)viewDidLoad
{
    [super viewDidLoad];
//    ArticleViewController *articlePreview = [[ArticleViewController alloc] init];
//    previewContainer.backgroundColor = [UIColor blackColor];
//    [self addChildViewController:articlePreview];
//    [self.view addSubview:articlePreview.view];
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://inforum.org.pt/INForum2012/docs/20120184.pdf"]];
    webView.dataDetectorTypes = UIDataDetectorTypeAll;
    
	[webView loadRequest:request];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)previewDocument2:(id)sender {
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
    
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        
        // Preview PDF
        [self.documentInteractionController presentPreviewAnimated:YES];
    }
}

@end
