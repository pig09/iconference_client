//
//  MSCalendarViewController.h
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2013 Monospace Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface MSCalendarViewController : UICollectionViewController<SlideNavigationControllerDelegate>
@property (nonatomic, strong) UIPopoverController *popover;
- (void) loadSchedule;
- (void) loadProgram;

@end
