//
//  MenuViewController.m
//  iConf
//
//  Created by João Rosa on 04/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "MenuViewController.h"
#import "MSCalendarViewController.h"
#import "User.h"
#import "MappingProvider.h"
#import "Author.h"
#import "Event.h"
#import "MapGalleryViewController.h"
#import "Event.h"
#import "Conference.h"


@implementation MenuViewController{
    User *user;
    NSMutableArray *presentations;
    NSMutableArray *authors;
    NSMutableArray *events;
    NSMutableDictionary *list;
    NSArray *firstSectionData;
    NSArray *firstSectionImages;
    
}

@synthesize tableData;
@synthesize tableImages;
@synthesize searchResults;
@synthesize searchResults1;
@synthesize searchResults2;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    user=[User sharedUser];

    tableData = [[NSArray alloc]initWithObjects:@"Page",@"Calendar", @"Maps",@"Authors",@"Presentations",nil ];
    tableImages = [[NSArray alloc]initWithObjects:@"btn-menu-conference.png",@"btn-menu-calendar.png", @"btn-menu-map.png",@"btn-menu-authors.png",@"btn-menu-presentations.png",nil ];
    
           
    firstSectionData = [[NSArray alloc]initWithObjects:@"Gallery", @"Logout",nil ];
    firstSectionImages = [[NSArray alloc]initWithObjects:@"btn-menu-gallery.png",@"btn-menu-logout.png",nil ];
    searchResults = [NSArray array];
    searchResults1 = [NSArray array];
    searchResults2 = [NSArray array];
    self.search =[NSArray array];
    self.search1 =[NSArray array];
    self.search2 =[NSArray array];
    
    
    
    [self setupSearchBar];
    
    
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    list = [[NSMutableDictionary alloc] init];
    
    
    NSManagedObjectContext *managed=user.managedObjectStore.persistentStoreManagedObjectContext;
    
    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"Author"];
    fetchRequest1.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    
    authors=[[managed executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
    NSMutableArray * cenas= [NSMutableArray array];
    for (Author *author in authors) {
        [cenas addObject:author.authorName];
    }
    
    NSArray *cleanedArray = [[NSSet setWithArray:cenas] allObjects];
    [list setObject:cleanedArray forKey:@"Authors"];
    
    
    NSManagedObjectContext *managed1=user.managedObjectStore.persistentStoreManagedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Presentation"];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    presentations=[[managed1 executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSMutableArray * cenas1= [NSMutableArray array];
    for (Presentation *pres in presentations) {
        [cenas1 addObject:pres.presTitle];
    }
    [list setObject:cenas1 forKey:@"Presentations"];
    
    
    NSManagedObjectContext *managed2=user.managedObjectStore.persistentStoreManagedObjectContext;
    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] initWithEntityName:@"Event"];
    fetchRequest2.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    fetchRequest2.predicate =[NSPredicate predicateWithFormat:@"NOT(title LIKE %@)",@"Branco"];
    events=[[managed2 executeFetchRequest:fetchRequest2 error:nil] mutableCopy];
    NSMutableArray * cenas3= [NSMutableArray array];
    for (Event *event in events) {
        [cenas3 addObject:event.title];
    }
    [list setObject:cenas3 forKey:@"Events"];
    

    return YES;
}


-(void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView {
    
    
    CGRect f = self.menuTable.frame;  // The tableView the search replaces
    CGRect s = self.searchDisplayController.searchBar.frame;
    CGRect newFrame = CGRectMake(f.origin.x,
                                 f.origin.y + s.size.height,
                                 f.size.width,
                                 f.size.height - s.size.height);
    
    
    tableView.frame = newFrame;
    
}

- (void)setupSearchBar {
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.menuTable.tableHeaderView=self.searchBar;
    
    self.searchController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar
                                                              contentsController:self];
    [self.searchController setValue:[NSNumber numberWithInt:UITableViewStyleGrouped]
                             forKey:@"_searchResultsTableViewStyle"];
    
    self.searchController.searchResultsDataSource = self;
    self.searchController.searchResultsDelegate = self;
    self.searchController.delegate = self;
    self.searchBar.delegate = self;
    
    CGPoint offset = CGPointMake(0, self.searchBar.frame.size.height);
    self.menuTable.contentOffset = offset;
    
    
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    
    searchResults =[list allValues][1];
    searchResults1 = [list allValues][2];
    searchResults2 = [list allValues][0];
    self.search= [searchResults filteredArrayUsingPredicate:resultPredicate];
    self.search1= [searchResults1 filteredArrayUsingPredicate:resultPredicate];
    self.search2= [searchResults2 filteredArrayUsingPredicate:resultPredicate];

    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}



//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
//
//        cell.contentView.backgroundColor = [UIColor colorWithRed:237 green:237 blue:237 alpha:1];
//              
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [list.allKeys count];
        
    } else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (section==0) {
            return [self.search count];
        }else if (section==1){
            return [self.search1 count];
        }else{
            return [self.search2 count];
        }
        
    } else {
        if(section==0){
            return [firstSectionData count];
        }else{
            return [tableData count];

        }
        
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if(section == 0)
        {
            return @"Authors";
        }
        else if(section==1)
        {
            return @"Presentations";
        }else{
            return @"Events";
        }
        
    } else {
        if (section ==0) {
            return @"";
        }else
            return @"Conference";
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell"];
    }
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (indexPath.section==0) {
            cell.textLabel.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:15];
            cell.textLabel.text = [self.search objectAtIndex:indexPath.row];
            
        }else if (indexPath.section==1){
            cell.textLabel.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:15];
            cell.textLabel.text = [self.search1 objectAtIndex:indexPath.row];
            
        }else if(indexPath.section==2){
            cell.textLabel.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:15];
            cell.textLabel.text = [self.search2 objectAtIndex:indexPath.row];
        }
    } else {
        if(indexPath.section==0){
            cell.textLabel.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:15];
            cell.textLabel.text=[firstSectionData objectAtIndex:indexPath.row];
            cell.imageView.image= [UIImage imageNamed:[firstSectionImages objectAtIndex:indexPath.row]];
        }else{
            cell.textLabel.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:15];
            cell.textLabel.text=[tableData objectAtIndex:indexPath.row];
            cell.imageView.image= [UIImage imageNamed:[tableImages objectAtIndex:indexPath.row]];

            
        }
    }
    
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                                 bundle: nil];
        
        UIViewController *vc ;
        NSIndexPath *indexPath;
        NSString *Name;
        
        if([[self tableView:tableView titleForHeaderInSection:[tableView.indexPathForSelectedRow section]] isEqualToString:@"Presentations"] )
        {
            indexPath = [tableView indexPathForSelectedRow];
            self.selectedPres = [self.search1 objectAtIndex:indexPath.row];
            Name= [NSString stringWithFormat:(@"%@"), self.selectedPres];
            int ident;
            int sess;
            for(Presentation *pres in presentations){
                if([pres.presTitle isEqualToString:Name]){
                    ident=[[pres valueForKey:@"presID"] intValue];
                    sess=[[pres valueForKey:@"sessID"] intValue];
                }
                
            }
            [self.searchBar resignFirstResponder];
            
            user.presId=ident;
            user.selectedSession=sess;
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"presentation"];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
            
            
            
        }else if([[self tableView:tableView titleForHeaderInSection:[tableView.indexPathForSelectedRow section]] isEqualToString:@"Authors"]){
            indexPath = [tableView indexPathForSelectedRow];
            self.selectedAuthor = [self.search objectAtIndex:indexPath.row];
            Name= [NSString stringWithFormat:(@"%@"), self.selectedAuthor];
            int identAuthor;
            for(Author *author in authors){
                if([author.authorName isEqualToString:Name]){
                    identAuthor=[[author valueForKey:@"authorId"] intValue];
                }
                
            }
            [self.searchBar resignFirstResponder];
            
            user.authorId=identAuthor;
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"author"];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            
            
            
            [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
        }else{
            indexPath = [tableView indexPathForSelectedRow];
            self.selectedEvent = [self.search2 objectAtIndex:indexPath.row];
            Name= [NSString stringWithFormat:(@"%@"), self.selectedEvent];
            int identEvent;
            for(Event *event in events){
                if([event.title isEqualToString:Name]){
                    identEvent=[[event valueForKey:@"eventID"] intValue];
                }
                
            }
            [self.searchBar resignFirstResponder];
            user.eventId=identEvent;
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"event"];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            
            
            
            [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
 
        }
        
    }else{
        
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                                 bundle: nil];
        
        UIViewController *vc ;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        if([[self tableView:tableView titleForHeaderInSection:[tableView.indexPathForSelectedRow section]] isEqualToString:@"Conference"] )
        {
        
        switch (indexPath.row )
        {
            case 0:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"ConferencePage"];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:nil];

                break;
                
            case 1:
                vc = [[MSCalendarViewController alloc] init];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:nil];

                break;
                
            case 2:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"mapGallery"];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
                break;
            case 3:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"authorList"];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
                break;
            case 4:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"presList"];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
                break;
        }
        }else{
            switch (indexPath.row )
            {
                case 0:
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"confGallery"];
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:nil];
                    
                    break;
                    
                case 1:
                    // saving an NSString
                    [prefs removeObjectForKey:@"UserID"];
                    [prefs removeObjectForKey:@"UserToken"];
                    [prefs removeObjectForKey:@"UserConf"];
                    [prefs setObject:@"invalid" forKey:@"ValidUser"];
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"login"];
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:nil];
                    break;
            }

        }
        
        
    }
    
}
    
    
    
    
    

@end



