//
//  EventInfoViewController.h
//  iConf
//
//  Created by João Rosa on 01/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventInfoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *eventName;
@property (strong, nonatomic) IBOutlet UILabel *eventDuration;
@property (strong, nonatomic) IBOutlet UILabel *eventLocal;
@property (strong, nonatomic) IBOutlet UITextView *eventNotes;
@property (strong, nonatomic) IBOutlet UILabel *eventStart;

@end
