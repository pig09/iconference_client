//
//  EventInfoViewController.m
//  iConf
//
//  Created by João Rosa on 01/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "EventInfoViewController.h"
#import "User.h"
#import "Event.h"

@interface EventInfoViewController ()

@end

@implementation EventInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    User *user =[User sharedUser];
    
    NSManagedObjectContext *moc =user.managedObjectStore.persistentStoreManagedObjectContext ;
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:moc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"eventID==%d",user.eventId];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    Event *event = [[moc executeFetchRequest:request error:nil] lastObject];
    
    request = nil;
    
    NSLog(@"EVENT NAME %@", event.title);
    self.eventName.text=event.title;
    self.eventDuration.text=[NSString stringWithFormat:@"%@ min",event.duration];
    self.eventLocal.text=event.local;
    self.eventNotes.text=event.notes;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd :HH-mm"];
    NSString *dateString = [dateFormatter stringFromDate:event.start];
    self.eventStart.text=dateString;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
