//
//  SignInViewController.h
//  iConf
//
//  Created by Daniel Dionisio on 03/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate>{
    IBOutlet UIScrollView *signScroll;
}


@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmationTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;


- (IBAction)signInButtonAction:(id)sender;

@end
