//
//  MappingProvider.m
//  
//
//  Created by Daniel Dionisio on 24/05/13.
//
//

#import "MappingProvider.h"
#import "Conference.h"
#import "Restkit/Restkit.h"
#import "MSEvent.h"
#import "User.h"
#import "Presentation.h"
#import "Author.h"
#import "Map.h"

@implementation MappingProvider

+ (RKMapping *)conferenceMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Conference class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"id" :@"confID",
     @"conf_date" :@"date",
     @"name" : @"confName",
     @"local" : @"confLocal",
     @"description" :@"confDescription",
     @"avatar": @"avatar"
     }];
    return mapping;
}

+ (RKMapping *)mapMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Map class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"id" :@"mapId",
     @"url" :@"mapUrl",
     @"name" : @"mapName"
     }];
    return mapping;
}

+ (RKMapping *)presentationMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Presentation class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"id" :@"presID",
     @"session_id" :@"sessID",
     @"title" :@"presTitle",
     @"article" : @"presArticle",
     @"room_name" : @"roomName",
     @"room_map" : @"roomMap",
     @"speaker" : @"speaker",
     @"start_at" : @"presStart",
     @"end_at" :@"presEnd"
     }];
    return mapping;
}

+ (RKMapping *)userMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[User class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"name":@"name",
     @"Lastname":@"familyName",
     @"Telephone":@"telephoneNumber",
     @"Institution":@"institutionID",
     @"Email":@"email",
     @"access_token":@"sessionToken",
     @"id":@"userID"
     }];
    return mapping;
}

+ (RKMapping *)authorMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Author class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"author_id":@"authorId",
     @"user_id":@"userId",
     @"name":@"authorName",
     @"institution":@"authorInstitution",
     @"email":@"authorEmail",
     @"photo":@"authorPhoto",
     }];
    return mapping;
}

+ (RKMapping *)scheduleMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[User class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"attendee_id":@"attendeeID",
     @"schedule_id":@"scheduleID"
     }];
    return mapping;
}


@end
