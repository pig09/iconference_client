//
//  MapGalleryViewController.m
//  iConf
//
//  Created by João Rosa on 27/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "MapGalleryViewController.h"
#import "Map.h"
#import "User.h"
#import "MapCell.h"
#import "SlideNavigationController.h"

@interface MapGalleryViewController (){
User *user;
}



@property (nonatomic, strong) NSMutableArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageImagesTemp;
@property (nonatomic, strong) NSMutableArray *mapNames;

@end

@implementation MapGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.pageImages=[[NSMutableArray alloc]init];
    self.mapNames=[[NSMutableArray alloc]init];

    self.pageImagesTemp=[[NSMutableArray alloc]init];
    user=[User sharedUser];
    self.title=@"Map Gallery";
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
        NSManagedObjectContext *managed=user.managedObjectStore.persistentStoreManagedObjectContext;
    
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Map"];
        self.pageImagesTemp = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    
        for(Map *map in self.pageImagesTemp){
            UIImage *image = [UIImage imageWithData:map.mapImg];
            [self.pageImages addObject:image];
            [self.mapNames addObject:map.mapName];
            NSLog(@"Number of maps: %d", self.pageImages.count);

        }
    
    //[self.pageImages addObject:[UIImage imageNamed:@"logo.png"]];
    
    NSLog(@"Number of maps: %d", self.pageImages.count);
    
//[self.mapCollection reloadData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.pageImages count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier=@"mapCell";
    MapCell *cell = [cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    //UIImage *img=[self.pageImages objectAtIndex:indexPath.row];
    //cell.mapImg.image = img;
    [[cell mapImg]setImage:[self.pageImages objectAtIndex:indexPath.item]];
    [[cell mapName]setText:[self.mapNames objectAtIndex:indexPath.item]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MapStoryboard" bundle: nil];
    
    UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"map"];
    
    MapCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
//    [UIView animateWithDuration:1.5 animations:^{
//        
//        //any animateable attribute here.
//        
//        cell.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
//        
//    } completion:^(BOOL finished) {
        user.mapImage=cell.mapImg.image;
        user.mapName=cell.mapName.text;
        
        [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES]; //}];
    
    
}


@end
