//
//  SignInViewController.m
//  iConf
//
//  Created by Daniel Dionisio on 03/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "SignInViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 264;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 352;

@interface SignInViewController ()
{
    CGFloat animatedDistance;
}

@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    signScroll.contentSize = signScroll.frame.size;
    
    NSLog(@"Height first :%f",signScroll.contentSize.height);
    NSLog(@"Width first :%f",signScroll.contentSize.width);
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    signScroll.contentOffset = CGPointZero;
}

//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    
//    NSLog(@"Height :%f",signScroll.contentSize.height);
//    NSLog(@"Width :%f",signScroll.contentSize.width);
//    
////    signScroll=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 500, 500)];
////    signScroll.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
//    //signScroll.delegate = self;
//    self.emailTextField.delegate = self;
//    self.passwordTextField.delegate=self;
//    self.passwordConfirmationTextField.delegate=self;
//	// Do any additional setup after loading the view.
//}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog(@"Entrou");
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft
        ||  toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        NSLog(@"Height :%f",signScroll.contentSize.height);
        NSLog(@"Width :%f",signScroll.contentSize.width);

       
        NSLog(@"Height :%f",signScroll.frame.size.height);
        NSLog(@"Width :%f",signScroll.frame.size.width);
        
//        self.passwordTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                                   UIViewAutoresizingFlexibleHeight);
//        self.emailTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                                UIViewAutoresizingFlexibleHeight);
//        self.firstNameTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                                    UIViewAutoresizingFlexibleHeight);
//        self.lastNameTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                                   UIViewAutoresizingFlexibleHeight);
//        signScroll.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleHeight);
//        self.passwordTextField.contentMode = UIViewContentModeScaleAspectFit;
//        signScroll.contentMode = UIViewContentModeCenter;
        
//        self.passwordTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleHeight);
//        self.emailTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleHeight);
//        self.firstNameTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleHeight);
//        self.lastNameTextField.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleHeight);
        
        //signScroll.contentMode = UIViewContentModeTopRight;
        
       // [self.emailTextField setAutoresizingMask: UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
//        
        //self.emailTextField.frame=CGRectMake(UIViewAutoresizingFlexibleLeftMargin, UIViewAutoresizingFlexibleTopMargin, UIViewAutoresizingFlexibleWidth, UIViewAutoresizingFlexibleHeight);
       //self.passwordTextField.frame=CGRectMake(self.passwordTextField.frame.origin.x+100, self.passwordTextField.frame.origin.y, self.passwordTextField.frame.size.width, self.passwordTextField.frame.size.height);
//        self.firstNameTextField.frame=CGRectMake(0, 350, self.firstNameTextField.frame.size.height, self.firstNameTextField.frame.size.height);
//        self.lastNameTextField.frame=CGRectMake(0, 350, self.lastNameTextField.frame.size.height, self.lastNameTextField.frame.size.height);
        
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInButtonAction:(id)sender {
    [self postUser];
}

//CODIGO LOGIN
-(void) postUser
{
    // make the dictionary
    NSDictionary *newUser = [[NSDictionary alloc]initWithObjectsAndKeys:
                             self.firstNameTextField.text, @"firstName",
                             self.lastNameTextField.text, @"lastName",
                             self.emailTextField.text, @"email",
                             self.emailTextField.text, @"email_confirmation",
                             self.passwordTextField.text, @"password",
                             self.passwordTextField.text, @"password_confirmation", 
                             nil];
    
    NSError *error;
    NSData *serializedJSON = [RKMIMETypeSerialization dataFromObject:newUser MIMEType:RKMIMETypeJSON error:&error];
    
    NSURL* url = [[NSURL alloc]initWithString:@"http://193.136.122.140"];
    RKObjectManager* manager = [RKObjectManager managerWithBaseURL:url];
    
    NSMutableURLRequest *request = [manager requestWithObject:nil method:RKRequestMethodPOST path:@"users/" parameters:nil];
    [request setValue:RKMIMETypeJSON forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:serializedJSON];
    
    RKObjectRequestOperation *operation = [manager objectRequestOperationWithRequest:request success:^ (RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Success block
       

    } failure:^ (RKObjectRequestOperation *operation, NSError *error) {
        // Failure block
    }];
    
    [manager enqueueObjectRequestOperation:operation];
    [self performSegueWithIdentifier:@"segueSignInToLogin" sender:self];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration successful"
                                                    message:@"Welcome aboard"                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
    

    // http://localhost:3000/rooms  -XPOST -H "Content-Type: application/json" -d '{"name":"Room 1","capacity":"50"}'
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =[self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    CGRect viewFrame = self.view.frame;
    
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationLandscapeLeft)
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
        viewFrame.origin.y -= animatedDistance;
    }
    
    if (orientation == UIInterfaceOrientationLandscapeRight)
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
        viewFrame.origin.y -= animatedDistance;
    }
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
}



- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationLandscapeLeft)
    {
        viewFrame.origin.y += animatedDistance;
    }
    
    if (orientation == UIInterfaceOrientationLandscapeRight)
    {
        viewFrame.origin.y += animatedDistance;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
