//
//  MSEventCell.h
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2013 Monospace Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MSEvent;

@interface MSEventCell : UICollectionViewCell

@property (nonatomic, weak) MSEvent *event;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *location;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) UIImageView *iview;


- (void)updateColors:(NSString*)type image:(UIImage *)img;
- (void)updateColors:(UIImage *)img;
- (void)clearColors;
- (void)imgToNil;






@end
