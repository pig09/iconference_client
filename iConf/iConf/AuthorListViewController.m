//
//  AuthorListViewController.m
//  iConf
//
//  Created by João Rosa on 02/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "AuthorListViewController.h"
#import "User.h"
#import "Author.h"
#import "AuthorCell.h"

@interface AuthorListViewController (){
    NSMutableArray *authors;
    NSMutableArray *idsAuthors;
}

@end

@implementation AuthorListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadDatafromServer];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDatafromServer
{
    self.title=@"Authors";
    User *user =[User sharedUser];
    
    authors=[[NSMutableArray alloc] init];
    
    NSManagedObjectContext *managedObjectContext = user.managedObjectStore.persistentStoreManagedObjectContext;
    
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Author" inManagedObjectContext:managedObjectContext];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    [request setEntity:entity];
    [request setPredicate:predicate];
    idsAuthors=[[managedObjectContext executeFetchRequest:request error:nil] mutableCopy];
   
    NSMutableArray * cenas= [NSMutableArray array];
    for (Author *author in idsAuthors) {
        [cenas addObject:[NSNumber numberWithInt:[[author valueForKey:@"userId"] intValue]]];
    }
    
    NSArray *cleanedArray = [[NSSet setWithArray:cenas] allObjects];
    
    for(NSNumber* num in cleanedArray ){
        
        NSFetchRequest *request1= [[NSFetchRequest alloc] init];
        NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"Author" inManagedObjectContext:managedObjectContext];
        request1.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
        NSPredicate *predicate1 =[NSPredicate predicateWithFormat:@"userId==%d",num.intValue];
        [request1 setEntity:entity1];
        [request1 setPredicate:predicate1];
        [authors addObject:[[managedObjectContext executeFetchRequest:request1 error:nil]lastObject]];
        [authors sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"authorName" ascending:YES]]];

        
    }
    [self.tableView reloadData];
        
}

- (void)configureCell:(AuthorCell *)cell forAuthor:(Author *)author
{   
    cell.authorName.text = author.authorName;
    cell.authorEmail.text = author.authorEmail;
    cell.authorInstitution.text = author.authorInstitution;
    cell.authorPhoto.image = [self imageForAuthor:author];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return authors.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"authorCell";
    AuthorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    Author *author = [authors objectAtIndex:indexPath.row];
    
    [self configureCell:cell forAuthor:author];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    User *user = [User sharedUser];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    
    UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"author"];
    
    AuthorCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    int ident;
    for(Author *author in authors){
        if([author.authorName isEqualToString:cell.authorName.text]){
            ident=[[author valueForKey:@"authorId"] intValue];
        }
        
    }
    user.authorId=ident;
    
    
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
    
    
}


- (UIImage *)imageForAuthor:(Author *)author
{
    if( author.authorPhoto != nil)
    {
        return [UIImage imageWithData:author.authorImg];
    }else
        return [UIImage imageNamed:@"bkg-author-placeholder.png"];
}

@end
