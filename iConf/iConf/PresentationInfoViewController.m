//
//  PresentationInfoViewController.m
//  iConf
//
//  Created by Bruno Candeias on 15/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PresentationInfoViewController.h"
#import "User.h"
#import "Presentation.h"
#import <SVProgressHUD.h>

@interface PresentationInfoViewController (){
    Presentation *presentation;
}

@property (strong, nonatomic) UIDocumentInteractionController *documentInteractionController;

@end

@implementation PresentationInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    if(![self connectedToInternet]){
        self.articleButton.enabled=NO;
    }else{
        self.articleButton.enabled=YES;
    }
    
    [ self loadPresentation];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)connectedToInternet
{
    NSURL *url=[NSURL URLWithString:@"http://193.136.122.140"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode]==200)?YES:NO;
}


- (void)loadPresentation
{
    User *user=[User sharedUser];
    
    NSManagedObjectContext *moc =user.managedObjectStore.persistentStoreManagedObjectContext ;
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    request.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Presentation" inManagedObjectContext:moc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"presID==%d",user.presId];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    presentation = [[moc executeFetchRequest:request error:nil] lastObject];
    self.presName.text = presentation.presTitle;
    self.roomName.text = presentation.roomName;
    self.roomMap.image= [UIImage imageWithData:presentation.roomMapImg];
    self.roomName.text=presentation.roomName;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd :HH-mm"];
    NSString *dateString = [dateFormatter stringFromDate:presentation.presStart];
    self.presStart.text= dateString;
    
    self.presDuration.text=[NSString stringWithFormat:@"%@ min",presentation.presDuration];
    
    
    if (presentation.presArticle == nil){
        self.articleButton.hidden = YES;
        self.notAvailableLabel.hidden=NO;
    }else{
        self.notAvailableLabel.hidden = YES;
        self.articleButton.hidden=NO;

    }
    
    if (presentation.chair == nil){
        self.chairName.text = @"Chair not available";

    }else{
        self.chairName.text= presentation.chair;
    }
    
    if (presentation.speaker == nil){
        self.speakerName.text = @"Speaker not available";
        
    }else{
        self.speakerName.text= presentation.speaker;
    }


    
}


- (IBAction)previewDocument:(id)sender {
    [SVProgressHUD show];
    NSLog(@"ARTICLE_URL: %@",presentation.presArticle);
    NSData *pdfData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",presentation.presArticle]]];
    
    //Store the Data locally as PDF File
    
    NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    
    User *user=[User sharedUser];
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:[NSString stringWithFormat:@"PDF%dPres.pdf", user.presId]];
    
    [pdfData writeToFile:filePath atomically:YES];
    
    
    //Now create Request for the file that was saved in your documents folder
    
    NSURL *URL = [NSURL fileURLWithPath:filePath];
    
    
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        
        // Preview PDF
        [self.documentInteractionController presentPreviewAnimated:YES];
        [SVProgressHUD dismiss];
        //        [self.documentInteractionController.]
    }
    
}


#pragma mark -
#pragma mark Document Interaction Controller Delegate Methods
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

@end
