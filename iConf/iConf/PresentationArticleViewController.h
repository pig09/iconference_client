//
//  PresentationArticleViewController.h
//  iConf
//
//  Created by Bruno Candeias on 16/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationArticleViewController : UIViewController <UIDocumentInteractionControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *previewContainer;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
