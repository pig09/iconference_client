//
//  PresentationInfoViewController.h
//  iConf
//
//  Created by Bruno Candeias on 15/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationInfoViewController : UIViewController <UIDocumentInteractionControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *presName;
@property (weak, nonatomic) IBOutlet UILabel *chairName;
@property (weak, nonatomic) IBOutlet UILabel *speakerName;
@property (weak, nonatomic) IBOutlet UILabel *roomName;
@property (weak, nonatomic) IBOutlet UIImageView *roomMap;
@property (weak, nonatomic) IBOutlet UILabel *presDuration;

@property (weak, nonatomic) IBOutlet UIButton *articleButton;
@property (weak, nonatomic) IBOutlet UILabel *notAvailableLabel;
@property (strong, nonatomic) IBOutlet UILabel *presStart;

- (IBAction)previewDocument:(id)sender;

@end
