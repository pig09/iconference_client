//
//  Map.h
//  iConf
//
//  Created by João Rosa on 27/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Map : NSManagedObject
@property (nonatomic) int mapId;
@property (strong, nonatomic) NSString *mapName;
@property (strong, nonatomic) NSData *mapImg;
@property (strong, nonatomic) NSString *mapUrl;

@end
