//
//  PresentationAuthorViewController.m
//  iConf
//
//  Created by Bruno Candeias on 17/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PresentationAuthorViewController.h"
#import "AuthorCell.h"
#import "Author.h"
#import "User.h"
#import "MappingProvider.h"
#import "SVProgressHUD.h"
#import "PersonDetailViewController.h"
#import "AuthorPres.h"

@interface PresentationAuthorViewController (){
    NSMutableArray *authors;
    
}


//@property (strong,nonatomic) NSMutableArray *authors;
@property (strong,nonatomic) Author *selectedAuthor;



@end

@implementation PresentationAuthorViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadDatafromServer];
    
}

- (void)loadDatafromServer
{
    User *user =[User sharedUser];
    
    NSManagedObjectContext *managedObjectContext = user.managedObjectStore.persistentStoreManagedObjectContext;
    
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"AuthorPres" inManagedObjectContext:managedObjectContext];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"presId==%d",user.presId];
    [request setEntity:entity];
    [request setPredicate:predicate];
    NSMutableArray *usersOfPres=[[managedObjectContext executeFetchRequest:request error:nil] mutableCopy];
    
    authors=[[NSMutableArray alloc] init];
    for(AuthorPres *authorPres in usersOfPres){
        
        NSFetchRequest *request1= [[NSFetchRequest alloc] init];
        NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"Author" inManagedObjectContext:managedObjectContext];
        request1.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
        NSPredicate *predicate1 =[NSPredicate predicateWithFormat:@"userId==%d",[[authorPres valueForKey:@"userId"] intValue]];
        [request1 setEntity:entity1];
        [request1 setPredicate:predicate1];
        
        [authors addObject:[[managedObjectContext executeFetchRequest:request1 error:nil]lastObject]];
        
    }
    
    [self.tableView reloadData];
    
    
}

- (void)configureCell:(AuthorCell *)cell forAuthor:(Author *)author
{
    cell.authorName.text = author.authorName;
    cell.authorEmail.text = author.authorEmail;
    cell.authorInstitution.text = author.authorInstitution;
    cell.authorPhoto.image = [self imageForAuthor:author];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return authors.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"authorCell";
    AuthorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    Author *author = [authors objectAtIndex:indexPath.row];
    
    [self configureCell:cell forAuthor:author];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    User *user = [User sharedUser];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    
    UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"author"];
    
    AuthorCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    int ident;
    for(Author *author in authors){
        if([author.authorName isEqualToString:cell.authorName.text]){
            ident=[[author valueForKey:@"authorId"] intValue];
        }
        
    }
    user.authorId=ident;
    
    
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
    
    
}


- (UIImage *)imageForAuthor:(Author *)author
{
    if( author.authorPhoto != nil)
    {
        return [UIImage imageWithData:author.authorImg];
    }else
        return [UIImage imageNamed:@"bkg-author-placeholder.png"];
}

@end
