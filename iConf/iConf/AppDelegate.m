//
//  AppDelegate.m
//  iConf
//
//  Created by Daniel Dionisio on 21/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"
#import "ConfGalleryViewController.h"
#import "MSCalendarViewController.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    User *user =[User sharedUser];
    
   UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                            bundle: nil];
	
	
	MenuViewController *leftMenu = (MenuViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Menu"];
    
	
	[SlideNavigationController sharedInstance].leftMenu = leftMenu;
    
    

    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    application.applicationIconBadgeNumber=0;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *myString = [prefs stringForKey:@"ValidUser"];
    NSString *confSelected =[prefs stringForKey:@"UserConf"];
    NSLog(@"VALIDADE APP %@", myString);
    
    if([myString isEqualToString:@"valid"]){
        if(confSelected==nil){
            NSLog(@"Entrou Gallery");
            user.userID=[[prefs stringForKey:@"UserID"]intValue];
            user.sessionToken=[prefs stringForKey:@"UserToken"];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                                     bundle: nil];
            UIViewController *vc= [mainStoryboard instantiateViewControllerWithIdentifier: @"confGallery"];
            [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:nil];
        }else{
            NSLog(@"Entrou Page");
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
            bundle: nil];
            
            user.userID=[[prefs stringForKey:@"UserID"]intValue];
            user.sessionToken=[prefs stringForKey:@"UserToken"];
            user.sessionConf=[[prefs stringForKey:@"UserConf"]intValue];
            RKManagedObjectStore *man;
            NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
            man = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
            [man createPersistentStoreCoordinator];
            
            NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
            NSLog(@"O path e %@", storePath);
            
            NSError *error;
            
            NSPersistentStore *persistentStore = [man addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
            
            
            NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
            
            [man createManagedObjectContexts];
            
            // Configure a managed object cache to ensure we do not create duplicate objects
            man.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:man.persistentStoreManagedObjectContext];
            
            
            user.managedObjectStore=man;

            

            NSLog(@"OBJECTSTORE %@",man);
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
//                                                                     bundle: nil];
            UIViewController *vc= [mainStoryboard instantiateViewControllerWithIdentifier: @"ConferencePage"];
            [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:nil];

        }
        
    }

    
    return YES;

    
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
	NSString *newToken = [deviceToken description];
	newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
	NSLog(@"My token is: %@", newToken);
        
	
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSMutableDictionary *test =[userInfo objectForKey:@"aps"];
        application.applicationIconBadgeNumber = [[test objectForKey:@"badge"] intValue] - 1;
    NSLog(@"BADGE: %@",[test objectForKey:@"badge"]);
    
    MSCalendarViewController *calendar=[[MSCalendarViewController alloc]init];
    ConfGalleryViewController * confG=[[ConfGalleryViewController alloc] init];
    
    
    NSString *string = [test objectForKey:@"alert"];
    NSString *str=[NSString new];
    if ([string rangeOfString:@"Presentation"].location != NSNotFound) {
        [confG defUser];
        [confG loadPresentations];
        [calendar loadSchedule];
        
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Notification"
                                                      message:[test objectForKey:@"alert"]
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert show];
        
    } else if([string rangeOfString:@"Event"].location != NSNotFound){
        [confG defUser];
        [confG loadEvents];
        [calendar loadSchedule];


        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Notification"
                                                      message:[test objectForKey:@"alert"]
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert show];


    }
        
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
