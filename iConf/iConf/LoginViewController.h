//
//  LoginViewController.h
//  iConf
//
//  Created by Daniel Dionisio on 28/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
#import "SlideNavigationController.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate,SlideNavigationControllerDelegate>{
    ACAccountStore *accountStore;
    
}
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)loginButton:(id)sender;

@end
