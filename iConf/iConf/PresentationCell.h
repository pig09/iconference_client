//
//  PresentationCell.h
//  iConf
//
//  Created by João Rosa on 02/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *presName;
@property (weak, nonatomic) IBOutlet UILabel *presRoom;
@property (weak, nonatomic) IBOutlet UILabel *presStart;
@property (weak, nonatomic) IBOutlet UILabel *presDuration;

@end
