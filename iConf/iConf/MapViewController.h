//
//  MapViewController.h
//  iConf
//
//  Created by Bruno Candeias on 18/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *imageArray;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@end
