//
//  PresentationViewController.m
//  iConf
//
//  Created by Bruno Candeias on 14/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PresentationViewController.h"

@interface PresentationViewController ()

//@property (strong,nonatomic) NSString presentationName;

@end

@implementation PresentationViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customize];
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self customize];
    }
    return self;
}

- (void)customize{
    UIImage *tabbarBg = [UIImage imageNamed:@"bkg-tabbar.png"];
    [self.tabBar setBackgroundImage:tabbarBg];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return NO;
}

@end
