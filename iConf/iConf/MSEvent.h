//
//  MSEvent.h
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2013 Monospace Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSEvent : NSManagedObject

@property (nonatomic) NSNumber *remoteID;
@property (nonatomic, strong) NSDate *start;
@property (nonatomic, strong) NSDate *end;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *local;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *confID;


- (NSDate *)day;

@end
