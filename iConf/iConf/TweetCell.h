//
//  TweetCell.h
//  iConf
//
//  Created by João Rosa on 19/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TweetCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *tweetAvatar;
@property (strong, nonatomic) IBOutlet UILabel *tweetText;

@property (strong, nonatomic) IBOutlet UILabel *tweetScreenName;
@property (strong, nonatomic) IBOutlet UILabel *tweetName;
@property (strong, nonatomic) IBOutlet UILabel *tweetData;

@end
