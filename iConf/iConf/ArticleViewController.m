//
//  ArticleViewController.m
//  iConf
//
//  Created by Bruno Candeias on 17/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "ArticleViewController.h"

@interface ArticleViewController ()

@property (strong, nonatomic) UIDocumentInteractionController *documentInteractionController;

@end

@implementation ArticleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self previewDocument];
}

-(void)previewDocument
{
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
    //NSURL *URL = [NSURL URLWithString:@"http://inforum.org.pt/INForum2012/docs/20120184.pdf"];
    
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        
        // Preview PDF
        [self.documentInteractionController presentPreviewAnimated:YES];
        
        //        [self.documentInteractionController.]
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark Document Interaction Controller Delegate Methods
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}


@end
