//
//  Event.m
//  iConf
//
//  Created by João Rosa on 30/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "Event.h"

@implementation Event

@dynamic confID;
@dynamic eventID;
@dynamic local;
@dynamic notes;
@dynamic title;
@dynamic duration;
@dynamic start;

@end
