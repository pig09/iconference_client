//
//  AppDelegate.h
//  iConf
//
//  Created by Daniel Dionisio on 21/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "MenuViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end
