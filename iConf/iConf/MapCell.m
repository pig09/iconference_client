//
//  MapCell.m
//  iConf
//
//  Created by João Rosa on 27/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "MapCell.h"

@implementation MapCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
