//
//  Presentation.m
//  iConf
//
//  Created by João Rosa on 09/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "Presentation.h"

@implementation Presentation

@dynamic presArticle;
@dynamic roomName;
@dynamic roomMap;
@dynamic chair;
@dynamic speaker;
@dynamic presTitle;
@dynamic presID;
@dynamic presStart;
@dynamic sessID;
@dynamic presEnd;
@dynamic presDuration;
@dynamic roomMapImg;
@dynamic confID;

@end
