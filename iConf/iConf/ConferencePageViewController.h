//
//  ConferencePageViewController.h
//  iConf
//
//  Created by João Rosa on 27/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Conference.h"
#import "SlideNavigationController.h"
#import "User.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>

@interface ConferencePageViewController : UIViewController<SlideNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    ACAccountStore *accountStore;
    
    
}
@property (weak, nonatomic) IBOutlet UILabel *confName;

@property (weak, nonatomic) IBOutlet UIImageView *confLogo;
@property (weak, nonatomic) IBOutlet UITextView *confDesc;
@property (weak, nonatomic) IBOutlet UILabel *confDates;
@property (nonatomic) Conference *confIdent;
@property (nonatomic) NSArray *confThis;

@property (strong, nonatomic) IBOutlet UITableView *tweetTable;

- (IBAction)conferenceMap:(id)sender;


- (IBAction)conferenceProgram:(id)sender;





@end
