//
//  ConfGalleryViewController.h
//  iConf
//
//  Created by Daniel Dionisio on 24/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface ConfGalleryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) User *user;
- (void) loadPresentations;
- (void) loadEvents;
-(void) defUser;

@end
