//
//  User.h
//  iConf
//
//  Created by Daniel Dionisio on 22/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *familyName;
@property (nonatomic) NSString *telephoneNumber;
@property (strong, nonatomic) NSString *email;
@property (nonatomic) NSInteger institutionID;
@property (strong, nonatomic) NSString *biography;
@property (strong, nonatomic) NSDictionary *userPreferences;
@property (nonatomic) int sessionConf;
@property (nonatomic) int userID;
@property (strong, nonatomic) NSString *sessionToken;
@property (nonatomic) NSInteger *attendeeID;
@property (nonatomic) NSInteger *scheduleID;
@property (nonatomic) UIPopoverController *pop;
@property (nonatomic) int presId;
@property (nonatomic) int authorId;
@property (nonatomic) int eventId;
@property (nonatomic) RKManagedObjectStore *managedObjectStore;
@property (strong, nonatomic) UIImage *mapImage;
@property (strong, nonatomic) NSString *mapName;






@property (nonatomic) int selectedSession;

+ (id)sharedUser;

@end