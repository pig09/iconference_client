//
//  MSCalendarViewController.m
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2013 Monospace Ltd. All rights reserved.
//

#import "MSCalendarViewController.h"
#import "MSCollectionViewCalendarLayout.h"
#import "MSEvent.h"
#import "ConfGalleryViewController.h"
#import "ConferencePageViewController.h"
#import "User.h"
#import <SVProgressHUD.h>

// Collection View
#import "MSGridline.h"
#import "MSTimeRowHeaderBackground.h"
#import "MSDayColumnHeaderBackground.h"
#import "MSEventCell.h"
#import "MSDayColumnHeader.h"
#import "MSTimeRowHeader.h"
#import "MSCurrentTimeIndicator.h"
#import "MSCurrentTimeGridline.h"

NSString * const MSEventCellReuseIdentifier = @"MSEventCellReuseIdentifier";
NSString * const MSDayColumnHeaderReuseIdentifier = @"MSDayColumnHeaderReuseIdentifier";
NSString * const MSTimeRowHeaderReuseIdentifier = @"MSTimeRowHeaderReuseIdentifier";

@interface MSCalendarViewController () <MSCollectionViewDelegateCalendarLayout, NSFetchedResultsControllerDelegate>{
    NSMutableArray * results;
    NSMutableArray *eventsToTick;
}

@property (nonatomic, strong) MSCollectionViewCalendarLayout *collectionViewLayout;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic, strong) UIBarButtonItem *edit;

@end

@implementation MSCalendarViewController
RKManagedObjectStore *managedObjectStore;
BOOL multiple;
BOOL inProgram;
NSMutableArray *toBeAdded;
User *user;

@synthesize popover;

- (id)init
{
    self.collectionViewLayout = [[MSCollectionViewCalendarLayout alloc] init];
    self.collectionViewLayout.delegate = self;
    self = [super initWithCollectionViewLayout:self.collectionViewLayout];
    multiple=NO;
    toBeAdded=[NSMutableArray array];
    inProgram=YES;
    user=[User sharedUser];
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    results=[[NSMutableArray alloc] init];
    
    NSArray * segControlItems = [[NSArray alloc]initWithObjects:@"Program", @"My Schedule",nil ];
    
    if(![self connectedToInternet]){
        self.edit = [[UIBarButtonItem alloc] initWithTitle:@"Add"
                                                     style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
        self.edit.enabled=NO;
    }else{
        self.edit = [[UIBarButtonItem alloc] initWithTitle:@"Add"
                                                 style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
    }
    self.navigationItem.rightBarButtonItem=self.edit;
    
    self.segmentControl = [[UISegmentedControl alloc] initWithItems:segControlItems];
    [self.segmentControl addTarget:self action:@selector(valueChanged:) forControlEvents: UIControlEventValueChanged];
    self.segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
    self.segmentControl.selectedSegmentIndex=0;
    
    self.navigationItem.titleView=self.segmentControl;
    
    self.navigationItem.rightBarButtonItem=self.edit;
    
    [self valueChanged:self.segmentControl];
    
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.collectionView registerClass:MSEventCell.class forCellWithReuseIdentifier:MSEventCellReuseIdentifier];
    [self.collectionView registerClass:MSDayColumnHeader.class forSupplementaryViewOfKind:MSCollectionElementKindDayColumnHeader withReuseIdentifier:MSDayColumnHeaderReuseIdentifier];
    [self.collectionView registerClass:MSTimeRowHeader.class forSupplementaryViewOfKind:MSCollectionElementKindTimeRowHeader withReuseIdentifier:MSTimeRowHeaderReuseIdentifier];
    
    // These are optional—if you don't want any of the decoration views, just don't register a class for it
    [self.collectionViewLayout registerClass:MSCurrentTimeIndicator.class forDecorationViewOfKind:MSCollectionElementKindCurrentTimeIndicator];
    [self.collectionViewLayout registerClass:MSCurrentTimeGridline.class forDecorationViewOfKind:MSCollectionElementKindCurrentTimeHorizontalGridline];
    [self.collectionViewLayout registerClass:MSGridline.class forDecorationViewOfKind:MSCollectionElementKindHorizontalGridline];
    [self.collectionViewLayout registerClass:MSTimeRowHeaderBackground.class forDecorationViewOfKind:MSCollectionElementKindTimeRowHeaderBackground];
    [self.collectionViewLayout registerClass:MSDayColumnHeaderBackground.class forDecorationViewOfKind:MSCollectionElementKindDayColumnHeaderBackground];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.collectionViewLayout scrollCollectionViewToClosetSectionToCurrentTimeAnimated:NO];
}

- (IBAction)valueChanged:(id)sender{
    [SVProgressHUD show];
    multiple=NO;
    self.collectionView.allowsMultipleSelection = NO;
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        if(![self connectedToInternet]){
            self.edit.title = @"Add";
            self.edit.enabled=NO;
        }else{
            self.edit.enabled=YES;
            self.edit.title = @"Add";
            [self.edit setStyle:UIBarButtonItemStylePlain];
        }
        inProgram=YES;
        [self loadProgram];
        [SVProgressHUD dismiss];

    }else{
        if(![self connectedToInternet]){
            self.edit.title = @"Remove";
            self.edit.enabled=NO;
        }else{
            self.edit.enabled=YES;
            self.edit.title = @"Remove";
            [self.edit setStyle:UIBarButtonItemStylePlain];
        }
        inProgram=NO;
        [self loadSchedule];
        [SVProgressHUD dismiss];
    }
    
}

- (IBAction)edit:(id)sender{
    
    
    if (multiple) {
        multiple = NO;
        self.collectionView.allowsMultipleSelection = NO;
        
        if(inProgram){
            NSMutableArray *toSchedule=[NSMutableArray array];
            
            for(MSEvent *event in toBeAdded){
                NSDictionary *addEvent = [[NSDictionary alloc]initWithObjectsAndKeys:
                                          event.type, @"type",
                                          event.remoteID, @"id",
                                          nil];
                
                [toSchedule addObject:addEvent];
                
            }
            NSError *error;
            NSData *serializedJSON = [RKMIMETypeSerialization dataFromObject:toSchedule MIMEType:RKMIMETypeJSON error:&error];
            
            NSString *strUser =[NSString stringWithFormat:(@"{\"conference_id\":%d,\"user_id\":%d,\"results\":"), user.sessionConf,user.userID];\
            
            NSMutableData *results = [strUser dataUsingEncoding:NSUTF8StringEncoding];
            NSData * teste= [@"}" dataUsingEncoding:NSUTF8StringEncoding];
            [results appendData:serializedJSON];
            [results appendData:teste];
            
            
            NSString *strData = [[NSString alloc]initWithData:results encoding:NSUTF8StringEncoding];
            
            NSURL* url = [[NSURL alloc]initWithString:@"http://193.136.122.140"];
            RKObjectManager* manager = [RKObjectManager managerWithBaseURL:url];
            
            NSMutableURLRequest *request = [manager requestWithObject:nil method:RKRequestMethodPUT path:@"schedules/add" parameters:nil];
            [request setValue:RKMIMETypeJSON forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:results];
            
            RKObjectRequestOperation *operation = [manager objectRequestOperationWithRequest:request success:^ (RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                [self loadSchedule];
                [self loadProgram];
                [SVProgressHUD showSuccessWithStatus:@"Success!"];
            } failure:^ (RKObjectRequestOperation *operation, NSError *error) {
                // Failure block
            }];
            
            [manager enqueueObjectRequestOperation:operation];
            if(![self connectedToInternet]){
                self.edit.title = @"Add";
                self.edit.enabled=NO;
            }else{
                self.edit.enabled=YES;
                self.edit.title = @"Add";
                [self.edit setStyle:UIBarButtonItemStylePlain];
            }
            
        }else{
            NSMutableArray *toSchedule=[NSMutableArray array];
            
            for(MSEvent *event in toBeAdded){
                NSDictionary *addEvent = [[NSDictionary alloc]initWithObjectsAndKeys:
                                          event.type, @"type",
                                          event.remoteID, @"id",
                                          nil];
                
                [toSchedule addObject:addEvent];
                
            }
            NSError *error;
            NSData *serializedJSON = [RKMIMETypeSerialization dataFromObject:toSchedule MIMEType:RKMIMETypeJSON error:&error];
            
            NSString *strUser =[NSString stringWithFormat:(@"{\"conference_id\":%d,\"user_id\":%d,\"results\":"), user.sessionConf,user.userID];\
            
            NSMutableData *results = [strUser dataUsingEncoding:NSUTF8StringEncoding];
            NSData * teste= [@"}" dataUsingEncoding:NSUTF8StringEncoding];
            [results appendData:serializedJSON];
            [results appendData:teste];
            
            
            NSString *strData = [[NSString alloc]initWithData:results encoding:NSUTF8StringEncoding];
            
            NSURL* url = [[NSURL alloc]initWithString:@"http://193.136.122.140"];
            RKObjectManager* manager = [RKObjectManager managerWithBaseURL:url];
            
            NSMutableURLRequest *request = [manager requestWithObject:nil method:RKRequestMethodDELETE path:@"schedules/remove" parameters:nil];
            [request setValue:RKMIMETypeJSON forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:results];
            
            RKObjectRequestOperation *operation = [manager objectRequestOperationWithRequest:request success:^ (RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                [SVProgressHUD showSuccessWithStatus:@"Success!"];
                
            } failure:^ (RKObjectRequestOperation *operation, NSError *error) {
                // Failure block
            }];
            
            [manager enqueueObjectRequestOperation:operation];
            [self loadSchedule];
            if(![self connectedToInternet]){
                self.edit.title = @"Remove";
                self.edit.enabled=NO;
            }else{
                self.edit.enabled=YES;
                self.edit.title = @"Remove";
                [self.edit setStyle:UIBarButtonItemStylePlain];
            }
            
            
            //do remove
        }
        
    }else{
        toBeAdded=[NSMutableArray array];
        multiple=YES;
        self.collectionView.allowsMultipleSelection = YES;
        if(![self connectedToInternet]){
            self.edit.title = @"Done";
            self.edit.enabled=NO;
        }else{
            self.edit.enabled=YES;
            self.edit.title = @"Done";
            [self.edit setStyle:UIBarButtonItemStyleDone];
        }
        for(NSIndexPath *indexPath in self.collectionView.indexPathsForSelectedItems) {
            [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
        }
        
        for(NSIndexPath *indexPath in self.collectionView.indexPathsForVisibleItems) {
            [self.collectionView indexPathsForVisibleItems];
        }

    }
    
}

#pragma mark - MSCalendarViewController

- (BOOL)connectedToInternet
{
    NSURL *url=[NSURL URLWithString:@"http://193.136.122.140"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode]==200)?YES:NO;
}

- (void) loadProgram {
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://193.136.122.140/conferences/%d", user.sessionConf]]];
    
    
    // Initialize managed object store
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    objectManager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"id" :@"remoteID",
                                            @"end_at" :@"end",
                                            @"name" : @"name",
                                            @"type" : @"type",
                                            @"local" :@"local",
                                            @"start_at": @"start",
                                            };
    
    RKEntityMapping *eventMapping = [RKEntityMapping mappingForEntityForName:@"ConfProgram" inManagedObjectStore:objectManager.managedObjectStore];
    
    eventMapping.identificationAttributes = @[@"remoteID",@"type"];
    
    
    
    [eventMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    RKResponseDescriptor *eventIndexResponseDescriptorSession = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping pathPattern:nil keyPath:@"sessions" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping pathPattern:nil keyPath:@"events" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [objectManager addResponseDescriptor:eventIndexResponseDescriptorSession];
    [objectManager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    [objectManager addFetchRequestBlock:^NSFetchRequest *(NSURL *URL) {
        RKPathMatcher *pathMatcher = [RKPathMatcher pathMatcherWithPattern:@""];
        NSDictionary *argsDict = nil;
        BOOL match = [pathMatcher matchesPath:[URL relativePath] tokenizeQueryStrings:NO parsedArguments:&argsDict];
        if (match) {
            return [NSFetchRequest fetchRequestWithEntityName:@"ConfProgram"];
        }
        return nil;
    }];
    
    [managedObjectStore createPersistentStoreCoordinator];
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:nil];
    [managedObjectStore createManagedObjectContexts];
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"ConfProgram"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"start" ascending:YES]];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:managedObjectStore.persistentStoreManagedObjectContext
                                                                          sectionNameKeyPath:@"day"
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
    [self.fetchedResultsController performFetch:nil];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"ConfProgram"];
    }
    
    [objectManager getObjectsAtPath:@"program.json" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result){
        
        NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"ConfProgram"];
        NSMutableArray *storeMap = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        for(MSEvent *event=(MSEvent *)[NSEntityDescription insertNewObjectForEntityForName:@"ConfProgram" inManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext] in storeMap){
            
            
            [event setConfID:[NSString stringWithFormat:@"%d", user.sessionConf]];
            [managed save:nil];
            
            
        }

        
        [self controllerDidChangeContent:self.fetchedResultsController];
        
        
    }
    failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [self controllerDidChangeContent:self.fetchedResultsController];

    }];
    
}

- (void) loadSchedule{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"http://193.136.122.140"]];
    
    // Initialize managed object store
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    objectManager.managedObjectStore = managedObjectStore;
    
    
    NSDictionary *confMappingDictionary = @{
                                            @"id" :@"remoteID",
                                            @"end_at" :@"end",
                                            @"name" : @"name",
                                            @"type" : @"type",
                                            @"local" :@"local",
                                            @"start_at": @"start",
                                            };
    
    RKEntityMapping *eventMapping = [RKEntityMapping mappingForEntityForName:@"Schedule" inManagedObjectStore:objectManager.managedObjectStore];
    
    eventMapping.identificationAttributes = @[@"remoteID",@"type"];
    
    
    [eventMapping addAttributeMappingsFromDictionary:confMappingDictionary];
    
    
    RKResponseDescriptor *eventIndexResponseDescriptorSession = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping pathPattern:nil keyPath:@"sessions" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKResponseDescriptor *eventIndexResponseDescriptorEvent = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping pathPattern:nil keyPath:@"events" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [objectManager addResponseDescriptor:eventIndexResponseDescriptorSession];
    [objectManager addResponseDescriptor:eventIndexResponseDescriptorEvent];
    
    
    [objectManager addFetchRequestBlock:^NSFetchRequest *(NSURL *URL) {
        RKPathMatcher *pathMatcher = [RKPathMatcher pathMatcherWithPattern:@""];
        NSDictionary *argsDict = nil;
        BOOL match = [pathMatcher matchesPath:[URL relativePath] tokenizeQueryStrings:NO parsedArguments:&argsDict];
        if (match) {
            return [NSFetchRequest fetchRequestWithEntityName:@"Schedule"];
        }
        return nil;
    }];
    
    [managedObjectStore createPersistentStoreCoordinator];
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"ConfProgram.sqlite"];
    NSLog(@"O path e %@", storePath);
    [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:nil];
    [managedObjectStore createManagedObjectContexts];
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];

    
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Schedule"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"start" ascending:YES]];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];

    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:managedObjectStore.persistentStoreManagedObjectContext
                                                                          sectionNameKeyPath:@"day"
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
    [self.fetchedResultsController performFetch:nil];
    
    if([self connectedToInternet]){
        [self deleObjDB:@"Schedule"];
    }
    
    [objectManager getObjectsAtPath:[NSString stringWithFormat:(@"schedules.json?conference_id=%d&user_id=%d"), user.sessionConf, user.userID] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result){
        

        NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Schedule"];
        NSMutableArray *storeMap = [[managed executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        for(MSEvent *event=(MSEvent *)[NSEntityDescription insertNewObjectForEntityForName:@"Schedule" inManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext] in storeMap){
            
            
            [event setConfID:[NSString stringWithFormat:@"%d", user.sessionConf]];
            [managed save:nil];
            
            
        }
        
        
        [self controllerDidChangeContent:self.fetchedResultsController];
        

        
        
    }
    failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [self controllerDidChangeContent:self.fetchedResultsController];

    }];
    
}

-(void)deleObjDB:(NSString *)entity{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *managed=managedObjectStore.persistentStoreManagedObjectContext;
    [allCars setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:managed]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [managed executeFetchRequest:allCars error:&error];
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [managed deleteObject:car];
    }
    NSError *saveError = nil;
    [managed save:&saveError];
    
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    User *user =[User sharedUser];
    
    NSManagedObjectContext *managedObjectContext = user.managedObjectStore.persistentStoreManagedObjectContext;
    
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Schedule" inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    NSMutableArray *eventsInSchedule=[[managedObjectContext executeFetchRequest:request error:nil] mutableCopy];
    
    NSFetchRequest *request1= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"ConfProgram" inManagedObjectContext:managedObjectContext];
    [request1 setEntity:entity1];
    NSMutableArray *eventsInProgram=[[managedObjectContext executeFetchRequest:request1 error:nil] mutableCopy];
    
    
    eventsToTick=[[NSMutableArray alloc] init];
    
    for(MSEvent *eventSchedule in eventsInSchedule){
        for(MSEvent *eventProgram in eventsInProgram){
            if(eventSchedule.remoteID==eventProgram.remoteID && [eventSchedule.type isEqualToString:eventProgram.type]){
                [eventsToTick addObject:eventSchedule];
            }
        }
        
    }

    [self.collectionViewLayout invalidateLayoutCache];
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.fetchedResultsController.sections.count;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSEventCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MSEventCellReuseIdentifier forIndexPath:indexPath];
    
    cell.event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    if(inProgram){
    
        BOOL isToTick=NO;
        
        for(MSEvent *eventTick in eventsToTick){
            if([cell.event.type isEqualToString:[eventTick valueForKey:@"type"]] && cell.event.remoteID.integerValue==[[eventTick valueForKey:@"remoteID"] integerValue]){
                isToTick=YES;
            }
        }
        
            if (isToTick) {
                
                if([cell.event.type isEqualToString:@"event"] && ![cell.event.name isEqualToString:@"Branco"]){
                    [cell updateColors:cell.event.type image:[UIImage imageNamed:@"tickMark.png"]];
                }else if([cell.event.name isEqualToString:@"Branco"]){
                    [cell clearColors];
                    
                }else{
                    [cell updateColors:[UIImage imageNamed:@"tickMark.png"]];
                }
            }else{
                if([cell.event.type isEqualToString:@"event"] && ![cell.event.name isEqualToString:@"Branco"]){
                    [cell updateColors:cell.event.type image:nil];
                }else if([cell.event.name isEqualToString:@"Branco"]){
                    [cell clearColors];
                    
                }else{
                    [cell updateColors:nil];
                }
            }        
    }else{
        if([cell.event.type isEqualToString:@"event"] && ![cell.event.name isEqualToString:@"Branco"]){
            [cell updateColors:cell.event.type image:nil];
        }else if([cell.event.name isEqualToString:@"Branco"]){
            [cell clearColors];
            
        }else{
            [cell updateColors:nil];
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MSEvent *selectedEvent = [self.fetchedResultsController objectAtIndexPath:indexPath];
    user.selectedSession=selectedEvent.remoteID.intValue;
       
    if (multiple) {
        [toBeAdded addObject:selectedEvent];
        
    }else{
        if ([selectedEvent.type isEqual:@"session"]) {
            
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController* popoverView = [storyboard instantiateViewControllerWithIdentifier:@"popView"];
            [popoverView viewDidLoad];
            
            CGRect clippedRect = CGRectMake(0,0,100,50);
            
            popover= [[UIPopoverController alloc]initWithContentViewController:popoverView];
            user.pop = popover;
            [self.popover presentPopoverFromRect:clippedRect inView:[self.collectionView cellForItemAtIndexPath:indexPath] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
            
        }else{
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController* vc;
            
            user.eventId=selectedEvent.remoteID.intValue;
            [collectionView deselectItemAtIndexPath:indexPath animated:YES];
            vc=[storyboard instantiateViewControllerWithIdentifier:@"event"];

            
            [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
  
        }
        
    }
    
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (multiple) {
        NSString *deSelectedRecipe = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [toBeAdded removeObject:deSelectedRecipe];
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view;
    if ([kind isEqualToString:MSCollectionElementKindDayColumnHeader]) {
        MSDayColumnHeader *dayColumnHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:MSDayColumnHeaderReuseIdentifier forIndexPath:indexPath];
        dayColumnHeader.day = [self.collectionViewLayout dateForDayColumnHeaderAtIndexPath:indexPath];
        view = dayColumnHeader;
    }
    else if ([kind isEqualToString:MSCollectionElementKindTimeRowHeader]) {
        MSTimeRowHeader *timeRowHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:MSTimeRowHeaderReuseIdentifier forIndexPath:indexPath];
        timeRowHeader.time = [self.collectionViewLayout dateForTimeRowHeaderAtIndexPath:indexPath];
        view = timeRowHeader;
    }
    return view;
}

#pragma mark - MSCollectionViewCalendarLayout

- (NSDate *)collectionView:(UICollectionView *)collectionView layout:(MSCollectionViewCalendarLayout *)collectionViewLayout dayForSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:section];
    MSEvent *event = sectionInfo.objects[0];
    return [event day];
}

- (NSDate *)collectionView:(UICollectionView *)collectionView layout:(MSCollectionViewCalendarLayout *)collectionViewLayout startTimeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSEvent *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return event.start;
}

- (NSDate *)collectionView:(UICollectionView *)collectionView layout:(MSCollectionViewCalendarLayout *)collectionViewLayout endTimeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSEvent *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    // Sports usually last about 3 hours, and SeatGeek doesn't provide an end time
    NSTimeInterval diff = [event.end timeIntervalSinceDate:event.start];
    
    return [event.start dateByAddingTimeInterval:(diff)];
}

- (NSDate *)currentTimeComponentsForCollectionView:(UICollectionView *)collectionView layout:(MSCollectionViewCalendarLayout *)collectionViewLayout
{
    return [NSDate date];
}


- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return NO;
}


@end
