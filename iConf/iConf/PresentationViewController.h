//
//  PresentationViewController.h
//  iConf
//
//  Created by Bruno Candeias on 14/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationViewController : UITabBarController

@end
