//
//  Conference.h
//  iConf
//
//  Created by Daniel Dionisio on 22/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Conference : NSManagedObject


@property (strong,nonatomic) NSString *confName;
@property (strong,nonatomic) NSString *confDescription;
@property (strong,nonatomic) NSString *confLocal;
@property (nonatomic) int confID;
@property (strong,nonatomic) NSString *date;
@property (nonatomic) int duration;
@property (strong,nonatomic) NSString *avatar;
@property (strong,nonatomic) NSString *hashtag;
@property (strong,nonatomic) NSData *confImg;



@end
