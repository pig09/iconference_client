//
//  PersonDetailViewController.m
//  iConf
//
//  Created by Bruno Candeias on 18/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//
#import "PersonDetailViewController.h"
#import "Author.h"
#import "User.h"
#import "Presentation.h"
#import "SlideNavigationController.h"
#import "AuthorPres.h"
@interface PersonDetailViewController ()

@property (strong,nonatomic) Author *author;
@property (nonatomic, strong) NSArray *authors;
@property (nonatomic, strong) NSMutableArray *articlesPres;



@end

@implementation PersonDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self){
        
    }
    return self;
}

- (void)viewDidLoad
{
    self.articlesPres=[[NSMutableArray alloc] init];
    [super viewDidLoad];
    [self loadAuthor];
}


- (void)loadAuthor
{
    
    User *user =[User sharedUser];
    NSManagedObjectContext *moc =user.managedObjectStore.persistentStoreManagedObjectContext ;
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Author" inManagedObjectContext:moc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"authorId==%d",user.authorId];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    
    Author *auth = [[moc executeFetchRequest:request error:nil] lastObject];
    
    self.name.text = auth.authorName;
    self.institution.text = auth.authorInstitution;
    self.email.text = auth.authorEmail;
    self.photo.image = [self imageForAuthor:auth];
    
    
    NSFetchRequest *request1= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"AuthorPres" inManagedObjectContext:moc];
    NSPredicate *predicate1 =[NSPredicate predicateWithFormat:@"userId==%d",[[auth valueForKey:@"userId"] intValue]];
    [request1 setEntity:entity1];
    [request1 setPredicate:predicate1];
    
    NSMutableArray *presArr= [[moc executeFetchRequest:request1 error:nil] mutableCopy];
        
    for(AuthorPres *authpres in presArr){

        int presID=[[authpres valueForKey:@"presId"] intValue];
        NSLog(@"PRESID %d",presID);
        NSManagedObjectContext *moc =user.managedObjectStore.persistentStoreManagedObjectContext ;

        NSFetchRequest *request2= [[NSFetchRequest alloc] init];
        NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"Presentation" inManagedObjectContext:moc];
        request2.predicate =[NSPredicate predicateWithFormat:@"presID== %d",presID];
        [request2 setEntity:entity2];
        
        [self.articlesPres addObject:[[moc executeFetchRequest:request2 error:nil]lastObject]];

    }
    
}

- (UIImage *)imageForAuthor:(Author *)author
{
    if( author.authorPhoto != nil)
    {
        return [UIImage imageWithData:author.authorImg];
    }else
        return [UIImage imageNamed:@"placeholder_dark.png"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.articlesPres count];
        
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
        return @"Articles";
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Presentation *pres=[self.articlesPres objectAtIndex:indexPath.row];
    cell.textLabel.text=[pres valueForKey:@"presTitle"];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    User *user = [User sharedUser];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    
    UIViewController *vc ;
    vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"presentation"];
    Presentation *pres =[self.articlesPres objectAtIndex:indexPath.row];
    user.presId=[[pres valueForKey:@"presID"] intValue];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
    
    
}



- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return NO;
}
@end

