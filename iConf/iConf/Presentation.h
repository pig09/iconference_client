//
//  Presentation.h
//  iConf
//
//  Created by João Rosa on 09/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Presentation : NSManagedObject

@property (strong,nonatomic) NSString *presArticle;
@property (strong,nonatomic) NSString *roomName;
@property (strong,nonatomic) NSString *roomMap;
@property (strong,nonatomic) NSString *chair;
@property (strong,nonatomic) NSString *speaker;
@property (strong,nonatomic) NSString *presTitle;
@property (strong,nonatomic) NSString *presDuration;
@property (nonatomic) int presID;
@property (strong,nonatomic) NSDate *presStart;
@property (nonatomic) int sessID;
@property (strong,nonatomic) NSDate *presEnd;
@property (strong,nonatomic) NSData *roomMapImg;
@property (strong,nonatomic) NSString *confID;

@end
