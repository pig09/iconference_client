//
//  PopoverViewController.h
//  iConf
//
//  Created by João Rosa on 09/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) int sessionID;

@end
