//
//  User.m
//  iConf
//
//  Created by Daniel Dionisio on 22/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "User.h"



@implementation User

#pragma mark Singleton Methods

+ (id)sharedUser {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}



- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
