//
//  MSEventCell.m
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2013 Monospace Ltd. All rights reserved.
//

#import "MSEventCell.h"
#import "MSEvent.h"

@interface MSEventCell (){
    
}


- (UIColor *)cellBackgroundColorSelected:(BOOL)selected;
- (UIColor *)cellTextColorSelected:(BOOL)selected;
- (UIColor *)cellBorderColorSelected:(BOOL)selected;
- (UIColor *)cellTextShadowColorSelected:(BOOL)selected;
- (CGSize)cellTextShadowOffsetSelected:(BOOL)selected;

- (void)updateColors:(NSString*)type image:(UIImage *)img;
- (void)updateColors:(UIImage *)img;
- (void)clearColors;
- (void)imgToNil;




@end

@implementation MSEventCell



#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        self.layer.shouldRasterize = YES;
        
        self.contentView.layer.borderWidth = 1.0;
        self.contentView.layer.cornerRadius = 4.0;
        self.contentView.layer.masksToBounds = YES;
        
        self.time = [UILabel new];
        self.time.backgroundColor = [UIColor clearColor];
        self.time.numberOfLines = 0;
        self.time.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:self.time];
        
        self.name = [UILabel new];
        self.name.backgroundColor = [UIColor clearColor];
        self.name.numberOfLines = 0;
        self.name.font = [UIFont boldSystemFontOfSize:12.0];
        [self.contentView addSubview:self.name];
        
        self.location = [UILabel new];
        self.location.backgroundColor = [UIColor clearColor];
        self.location.numberOfLines = 0;
        self.location.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:self.location];
        self.iview = [UIImageView new];
        [self.contentView addSubview:self.iview];
        
        self.type= [NSString new];
                
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
        
    UIEdgeInsets padding = UIEdgeInsetsMake(4.0, 5.0, 4.0, 5.0);
    CGFloat contentMargin = 2.0;
    CGFloat contentWidth = (CGRectGetWidth(self.contentView.frame) - padding.left - padding.right);
    
    CGSize maxTimeSize = CGSizeMake(contentWidth, CGRectGetHeight(self.contentView.frame) - padding.top - padding.bottom);
    CGSize timeSize = [self.time.text sizeWithFont:self.time.font constrainedToSize:maxTimeSize lineBreakMode:self.time.lineBreakMode];
    CGRect timeFrame = self.time.frame;
    timeFrame.size = timeSize;
    timeFrame.origin.x = padding.left;
    timeFrame.origin.y = padding.top;
    self.time.frame = timeFrame;
    
    CGSize maxTitleSize = CGSizeMake(contentWidth, CGRectGetHeight(self.contentView.frame) - (CGRectGetMaxY(timeFrame) + contentMargin) - padding.bottom);
    CGSize titleSize = [self.name.text sizeWithFont:self.name.font constrainedToSize:maxTitleSize lineBreakMode:self.name.lineBreakMode];
    CGRect titleFrame = self.name.frame;
    titleFrame.size = titleSize;
    titleFrame.origin.x = padding.left;
    titleFrame.origin.y = (CGRectGetMaxY(timeFrame) + contentMargin);
    self.name.frame = titleFrame;
    
    CGSize maxLocationSize = CGSizeMake(contentWidth, CGRectGetHeight(self.contentView.frame) - (CGRectGetMaxY(titleFrame) + contentMargin) - padding.bottom);
    CGSize locationSize = [self.location.text sizeWithFont:self.location.font constrainedToSize:maxLocationSize lineBreakMode:self.location.lineBreakMode];
    CGRect locationFrame = self.location.frame;
    locationFrame.size = locationSize;
    locationFrame.origin.x = padding.left;
    locationFrame.origin.y = (CGRectGetMaxY(titleFrame) + contentMargin);
    self.location.frame = locationFrame;
    

    CGRect ImageFrame = CGRectMake(contentWidth-(5*contentMargin),contentMargin,15,15);
    self.iview.frame = ImageFrame;
        
    
}


#pragma mark - UICollectionViewCell

- (void)setSelected:(BOOL)selected
{
    if (selected && self.selected != selected) {
        [UIView animateWithDuration:0.1 animations:^{
            self.transform = CGAffineTransformMakeScale(1.05, 1.05);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                self.transform = CGAffineTransformIdentity;
            }];
        }];
        
    }
        [super setSelected:selected];
    if(self.iview.image==nil){
        if([self.type isEqualToString:@"event"] && ![self.name.text isEqualToString:@"Branco"]){
            [self updateColors:self.type image:nil];
        }else if([self.name.text isEqualToString:@"Branco"]){
            [self clearColors];
        }else{
            [self updateColors:nil];
        }

    }else{
        if([self.type isEqualToString:@"event"] && ![self.name.text isEqualToString:@"Branco"]){
            [self updateColors:self.type image:[UIImage imageNamed:@"tickMark.png"]];
        }else if([self.name.text isEqualToString:@"Branco"]){
            [self clearColors];
        }else{
            [self updateColors:[UIImage imageNamed:@"tickMark.png"]];
        }
    }

}
    

#pragma mark - MSEventCell

- (void)setEvent:(MSEvent *)event
{
    _event = event;
//    NSLog(@"SetEvent");

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"h:mm a";
    
    self.time.text = [dateFormatter stringFromDate:event.start];
    self.name.text = event.name;
    self.location.text = event.local;
    self.type=event.type;
    
    //self.iview=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tickMark.png"]];
    
    
    [self setNeedsLayout];
    
}

- (void)updateColors:(NSString *)type image:(UIImage *)img
{
    self.contentView.backgroundColor = [self cellBackgroundColorSelectedEvent:self.selected];
    self.contentView.layer.borderColor = [[self cellBorderColorSelectedEvent:self.selected] CGColor];

    self.iview.image=img;

    self.time.textColor = [self cellTextColorSelectedEvent:self.selected];
    self.time.shadowColor = [self cellTextShadowColorSelectedEvent:self.selected];
    self.time.shadowOffset = [self cellTextShadowOffsetSelectedEvent:self.selected];
    
    self.name.textColor = [self cellTextColorSelectedEvent:self.selected];
    self.name.shadowColor = [self cellTextShadowColorSelectedEvent:self.selected];
    self.name.shadowOffset = [self cellTextShadowOffsetSelectedEvent:self.selected];
    
    self.location.textColor = [self cellTextColorSelectedEvent:self.selected];
    self.location.shadowColor = [self cellTextShadowColorSelectedEvent:self.selected];
    self.location.shadowOffset = [self cellTextShadowOffsetSelectedEvent:self.selected];
    
}


- (void)updateColors:(UIImage *)img{
    self.contentView.backgroundColor = [self cellBackgroundColorSelected:self.selected];
    self.contentView.layer.borderColor = [[self cellBorderColorSelected:self.selected] CGColor];
    
    self.iview.image=img;
    self.time.textColor = [self cellTextColorSelected:self.selected];
    self.time.shadowColor = [self cellTextShadowColorSelected:self.selected];
    self.time.shadowOffset = [self cellTextShadowOffsetSelected:self.selected];
    
    self.name.textColor = [self cellTextColorSelected:self.selected];
    self.name.shadowColor = [self cellTextShadowColorSelected:self.selected];
    self.name.shadowOffset = [self cellTextShadowOffsetSelected:self.selected];
    
    self.location.textColor = [self cellTextColorSelected:self.selected];
    self.location.shadowColor = [self cellTextShadowColorSelected:self.selected];
    self.location.shadowOffset = [self cellTextShadowOffsetSelected:self.selected];
}

- (void)clearColors{
    self.contentView.backgroundColor = [UIColor clearColor];
    self.contentView.layer.borderColor = [[UIColor clearColor]CGColor];
    self.iview.image=nil;
    
    self.time.textColor = [UIColor clearColor];
    self.time.shadowColor = [UIColor clearColor];
    self.time.shadowOffset = CGSizeMake(0.0, 0.0);
    
    self.name.textColor = [UIColor clearColor];
    self.name.shadowColor = [UIColor clearColor];
    self.name.shadowOffset = CGSizeMake(0.0, 0.0);
    
    self.location.textColor = [UIColor clearColor];
    self.location.shadowColor = [UIColor clearColor];
    self.location.shadowOffset = CGSizeMake(0.0, 0.0);
}

- (void)imgToNil{
    self.iview.image=nil;
}


- (UIColor *)cellBackgroundColorSelected:(BOOL)selected
{

return selected ? [[UIColor colorWithHexString:@"165b9b"] colorWithAlphaComponent:0.8]:[[UIColor colorWithHexString:@"b4d0ea"] colorWithAlphaComponent:0.8];
        
}

- (UIColor *)cellTextColorSelected:(BOOL)selected
{
    return selected ? [UIColor whiteColor] : [UIColor colorWithHexString:@"2b77ad"];
}

- (UIColor *)cellBorderColorSelected:(BOOL)selected
{
    return selected ? [UIColor colorWithHexString:@"0c2e4d"] : [UIColor colorWithHexString:@"2b77ad"];
}

- (UIColor *)cellTextShadowColorSelected:(BOOL)selected
{
    return selected ? [[UIColor blackColor] colorWithAlphaComponent:0.5] : [[UIColor whiteColor] colorWithAlphaComponent:0.5];
}

- (CGSize)cellTextShadowOffsetSelected:(BOOL)selected
{
    return selected ? CGSizeMake(0.0, -1.0) : CGSizeMake(0.0, 1.0);
}




- (UIColor *)cellBackgroundColorSelectedEvent:(BOOL)selected
{
    
    return selected ? [[UIColor colorWithHexString:@"FFCC00"] colorWithAlphaComponent:0.8]:[[UIColor colorWithHexString:@"FFFF99"] colorWithAlphaComponent:0.8];
    
}

- (UIColor *)cellTextColorSelectedEvent:(BOOL)selected
{
    return selected ? [UIColor blackColor] : [UIColor brownColor];
}

- (UIColor *)cellBorderColorSelectedEvent:(BOOL)selected
{
    return selected ? [UIColor colorWithHexString:@"0c2e4d"] : [UIColor colorWithHexString:@"2b77ad"];
}

- (UIColor *)cellTextShadowColorSelectedEvent:(BOOL)selected
{
    return selected ? [[UIColor brownColor] colorWithAlphaComponent:0.5] : [[UIColor whiteColor] colorWithAlphaComponent:0.5];
}

- (CGSize)cellTextShadowOffsetSelectedEvent:(BOOL)selected
{
    return selected ? CGSizeMake(0.0, -1.0) : CGSizeMake(0.0, 1.0);
}





@end
