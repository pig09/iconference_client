//
//  AuthorPres.h
//  iConf
//
//  Created by João Rosa on 25/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface AuthorPres : NSManagedObject

@property (nonatomic) int presId;
@property (nonatomic) int userId;

@end
