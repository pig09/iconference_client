//
//  ConferencePageViewController.m
//  iConf
//
//  Created by João Rosa on 27/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "ConferencePageViewController.h"
#import "Conference.h"
#import "AFNetworking.h"
#import <RestKit.h>
#import "MappingProvider.h"
#import "SVProgressHUD.h"
#import "MSCalendarViewController.h"
#import "User.h"
#import <QuartzCore/QuartzCore.h>
#import "TweetCell.h"
#import "MapViewController.h"

@interface ConferencePageViewController (){
    NSArray *status;
    NSString *hashtag;
}

@end

@implementation ConferencePageViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    User *user=[User sharedUser];
    hashtag=[NSString new];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *tempInt= [NSString stringWithFormat:@"%d", user.sessionConf];
    [prefs setObject:tempInt forKey:@"UserConf"];
    
    
    [self.confDesc.layer setMasksToBounds:YES];
    [self.confDesc.layer  setCornerRadius:5.0];

    accountStore = [[ACAccountStore alloc] init];
    [self loadDatafromServer];
    [self loadTwitterTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)loadDatafromServer //Codigo do exemplo twitter
{
    User *user =[User sharedUser];
    
        NSManagedObjectContext *moc =user.managedObjectStore.persistentStoreManagedObjectContext ;
        NSFetchRequest *request= [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Conference" inManagedObjectContext:moc];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"confID==%d",user.sessionConf];
        [request setEntity:entity];
        [request setPredicate:predicate];
        
        Conference *confrrrr = [[moc executeFetchRequest:request error:nil] lastObject];
        
        request = nil;

        hashtag=confrrrr.hashtag;
        self.confName.text=confrrrr.confName;
        self.confDates.text=confrrrr.date;
        self.confDesc.text=confrrrr.confDescription;
}


-(void)loadTwitterTable //Codigo do exemplo twitter
{
    
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error){
        if (granted) {
            
            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
            
            // Check if the users has setup at least one Twitter account
            
            if (accounts.count > 0)
            {
                ACAccount *twitterAccount = [accounts objectAtIndex:0];
                NSString *str=[NSString stringWithFormat:@"%@23%@",@"%",hashtag];
                // Creating a request to get the info about a user on Twitter
                NSLog(@"HASHTAG %@",str);
                SLRequest *twitterInfoRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:[NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json"] parameters:[NSDictionary dictionaryWithObject:str forKey:@"q"]];
                [twitterInfoRequest setAccount:twitterAccount];
                
                // Making the request
                
                [twitterInfoRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (responseData) {
                            
                            NSError *error = nil;
                            NSArray *TWData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                            
                            status=TWData;
                            [self.tweetTable reloadData];
                        }
                    });
                }];
            }
        } else {
            NSLog(@"No access granted");
        }
    }];
}


#pragma mark UITableViewDataSource methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}


- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return [[(NSDictionary *)status objectForKey:@"statuses"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = @"tweetCell";
    TweetCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    NSString *lastTweet = [[[(NSDictionary *)status objectForKey:@"statuses"] objectAtIndex:indexPath.row] objectForKey:@"text"];
    NSString *dataPost = [[[(NSDictionary *)status objectForKey:@"statuses"] objectAtIndex:indexPath.row] objectForKey:@"created_at"];
    NSString *screenName = [[[[(NSDictionary *)status objectForKey:@"statuses"] objectAtIndex:indexPath.row] objectForKey:@"user"]objectForKey:@"screen_name"];
    NSString *Username = [[[[(NSDictionary *)status objectForKey:@"statuses"] objectAtIndex:indexPath.row] objectForKey:@"user"]objectForKey:@"name"];
    NSString *photo = [[[[(NSDictionary *)status objectForKey:@"statuses"] objectAtIndex:indexPath.row] objectForKey:@"user"]objectForKey:@"profile_image_url"];
    photo = [photo stringByReplacingOccurrencesOfString:@"_normal"
                                             withString:@"_reasonably_small"];
    
    NSURL *imageUrl = [NSURL URLWithString:photo ];
    
    cell.tweetName.text = Username;
    cell.tweetScreenName.text = [NSString stringWithFormat:@" @%@", screenName];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"eee MMM dd HH:mm:ss +0000 ZZZZ"];
    NSDate *cellDate = [dateFormatter dateFromString:dataPost];
    [dateFormatter setDateFormat:@"MMM dd ZZZZ"];
    NSString *dateStr = [dateFormatter stringFromDate:cellDate];

    cell.tweetData.text = dateStr;
    [cell.tweetAvatar.layer setMasksToBounds:YES];
    //[cell.tweetAvatar.layer  setCornerRadius:39.0];
    [cell.tweetAvatar.layer  setCornerRadius:5.0];
    [cell.tweetAvatar setImageWithURL:imageUrl];
    cell.tweetText.text = lastTweet;
    
    return cell;
}


- (IBAction)conferenceMap:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    
    UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"mapGallery"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)conferenceProgram:(id)sender {
    MSCalendarViewController *calendar = [[MSCalendarViewController alloc] init];
    [self.navigationController pushViewController:calendar animated:YES];
    
}



- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return NO;
}

@end
