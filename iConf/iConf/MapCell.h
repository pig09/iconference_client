//
//  MapCell.h
//  iConf
//
//  Created by João Rosa on 27/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *mapImg;
@property (strong, nonatomic) IBOutlet UILabel *mapName;

@end
