//
//  PresentationAuthorViewController.h
//  iConf
//
//  Created by Bruno Candeias on 17/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface PresentationAuthorViewController : UITableViewController<SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@end
