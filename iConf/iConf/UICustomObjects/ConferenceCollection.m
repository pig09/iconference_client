//
//  ConferenceCollection.m
//  iConf
//
//  Created by Daniel Dionisio on 21/05/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "ConferenceCollection.h"

@implementation ConferenceCollection

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
