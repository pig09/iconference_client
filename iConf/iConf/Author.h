//
//  Author.h
//  iConf
//
//  Created by Bruno Candeias on 17/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Author : NSManagedObject

@property (nonatomic) int authorId;
@property (nonatomic) int userId;
@property (strong, nonatomic) NSString *authorName;
@property (strong, nonatomic) NSString *authorPhoto;
@property (strong, nonatomic) NSString *authorEmail;
@property (strong, nonatomic) NSString *authorInstitution;
@property (strong, nonatomic) NSString *authorHomepage;
@property (strong, nonatomic) NSData *authorImg;
@property (strong,nonatomic) NSString *confID;






@end
