//
//  MenuViewController.h
//  iConf
//
//  Created by João Rosa on 04/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "Presentation.h"
#import "Author.h"
#import "Event.h"

@interface MenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate>{
    NSArray *tableData;
    NSArray *tableImages;
    

}
@property (nonatomic, strong) NSArray *presentations;
@property (nonatomic, strong) NSArray *authors;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *searchController;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) NSArray *searchResults1;
@property (nonatomic, strong) NSArray *searchResults2;
@property (nonatomic, strong) NSArray *search;
@property (nonatomic, strong) NSArray *search1;
@property (nonatomic, strong) NSArray *search2;
@property (nonatomic, strong) NSMutableDictionary *list;
@property (nonatomic, strong) Event *selectedEvent;
@property (nonatomic, strong) Presentation *selectedPres;
@property (nonatomic, strong) Author *selectedAuthor;
@property (weak, nonatomic) IBOutlet UITableView *menuTable;


@property (nonatomic, retain) NSArray *tableData;
@property (nonatomic, retain) NSArray *tableImages;



@end
