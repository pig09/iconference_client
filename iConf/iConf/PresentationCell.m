//
//  PresentationCell.m
//  iConf
//
//  Created by João Rosa on 02/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PresentationCell.h"

@implementation PresentationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
