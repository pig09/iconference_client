//
//  AuthorListViewController.h
//  iConf
//
//  Created by João Rosa on 02/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface AuthorListViewController : UITableViewController<SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>


@end
