//
//  PopoverViewController.m
//  iConf
//
//  Created by João Rosa on 09/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PopoverViewController.h"
#import "MappingProvider.h"
#import "Presentation.h"
#import "PresentationViewController.h"
#import "SlideNavigationController.h"
#import "User.h"

@interface PopoverViewController (){
    NSArray *presentations;
}

@property (weak, nonatomic) IBOutlet UITableView *presTable;



@end

@implementation PopoverViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initwithSessionID: (int)sessionID
{
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadPresentations];
}

- (void)loadPresentations
{
    User *user=[User sharedUser];
    
    NSManagedObjectContext *moc =user.managedObjectStore.persistentStoreManagedObjectContext ;
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Presentation" inManagedObjectContext:moc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"sessID==%d",user.selectedSession];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    
    presentations = [[moc executeFetchRequest:request error:nil] mutableCopy];
    [self.presTable reloadData];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return presentations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Presentation *pres=[presentations objectAtIndex:indexPath.row];
    cell.textLabel.text=pres.presTitle;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    User *user = [User sharedUser];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    
    UIViewController *vc ;
    vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"presentation"];
    
    [user.pop dismissPopoverAnimated:YES];
    
    NSString *presentation_name = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    
    int ident;
    int sess;
    for(Presentation *pres in presentations){
        if([pres.presTitle isEqualToString:presentation_name]){
            ident=[[pres valueForKey:@"presID"] intValue];
            sess=[[pres valueForKey:@"sessID"] intValue];
        }
        
    }
    
    user.presId=ident;
    user.selectedSession=sess;
    
    
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
    
    
}



@end
