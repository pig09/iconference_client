//
//  PresentationListViewController.m
//  iConf
//
//  Created by João Rosa on 02/07/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "PresentationListViewController.h"
#import "Presentation.h"
#import "User.h"
#import "PresentationCell.h"

@interface PresentationListViewController (){
    NSMutableArray *presentations;
}

@end

@implementation PresentationListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadDatafromServer];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDatafromServer
{
    self.title=@"Presentations";
    User *user =[User sharedUser];
    
    NSManagedObjectContext *managedObjectContext = user.managedObjectStore.persistentStoreManagedObjectContext;
    
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Presentation" inManagedObjectContext:managedObjectContext];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"confID LIKE %@",[NSString stringWithFormat:@"%d", user.sessionConf]];
    
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"presTitle" ascending:YES]];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    presentations=[[managedObjectContext executeFetchRequest:request error:nil] mutableCopy];
    
    
    [self.tableView reloadData];
    
    
}

- (void)configureCell:(PresentationCell *)cell forPres:(Presentation *)pres
{
    cell.presName.text = pres.presTitle;
    cell.presRoom.text = pres.roomName;
    cell.presDuration.text = [NSString stringWithFormat:@"%@ min",pres.presDuration];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd :HH-mm"];
    NSString *dateString = [dateFormatter stringFromDate:pres.presStart];
    cell.presStart.text = dateString;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return presentations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"presCell";
    PresentationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    Presentation *pres = [presentations objectAtIndex:indexPath.row];
    
    [self configureCell:cell forPres:pres];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    User *user = [User sharedUser];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    
    UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier: @"presentation"];
    
    PresentationCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    int ident;
    for(Presentation *pres in presentations){
        if([pres.presTitle isEqualToString:cell.presName.text]){
            ident=[[pres valueForKey:@"presID"] intValue];
        }
        
    }
    user.presId=ident;
    
    
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
    
    
}

@end