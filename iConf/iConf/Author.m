//
//  Author.m
//  iConf
//
//  Created by Bruno Candeias on 17/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "Author.h"

@implementation Author

@dynamic authorId;
@dynamic userId;
@dynamic authorName;
@dynamic authorPhoto;
@dynamic authorEmail;
@dynamic authorInstitution;
@dynamic authorHomepage;
@dynamic authorImg;
@dynamic confID;


@end
