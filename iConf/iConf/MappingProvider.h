//
//  MappingProvider.h
//  
//
//  Created by Daniel Dionisio on 24/05/13.
//
//

#import <Foundation/Foundation.h>
#import "Restkit/Restkit.h"

@interface MappingProvider : NSObject

+(RKMapping *)conferenceMapping;
+(RKMapping *)userMapping;
+(RKMapping *)presentationMapping;
+(RKMapping *)scheduleMapping;
+(RKMapping *)authorMapping;
+ (RKMapping *)mapMapping;



@end
