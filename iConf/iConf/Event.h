//
//  Event.h
//  iConf
//
//  Created by João Rosa on 30/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Event : NSManagedObject

@property (nonatomic) int confID;
@property (nonatomic) int eventID;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *local;
@property (strong, nonatomic) NSString *notes;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSDate *start;

@end
