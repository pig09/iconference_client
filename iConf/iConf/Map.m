//
//  Map.m
//  iConf
//
//  Created by João Rosa on 27/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import "Map.h"

@implementation Map


@dynamic mapId;
@dynamic mapName;
@dynamic mapUrl;
@dynamic mapImg;
@end
