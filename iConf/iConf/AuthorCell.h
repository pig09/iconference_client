//
//  AuthorCell.h
//  iConf
//
//  Created by Bruno Candeias on 17/06/13.
//  Copyright (c) 2013 G09_PI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *authorPhoto;
@property (weak, nonatomic) IBOutlet UILabel *authorName;
@property (weak, nonatomic) IBOutlet UILabel *authorInstitution;
@property (weak, nonatomic) IBOutlet UILabel *authorHomepage;
@property (weak, nonatomic) IBOutlet UILabel *authorEmail;

@end
